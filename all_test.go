//  Copyright 2021 The Expat-Go Authors. All rights reserved.
//  Use of this source code is governed by a BSD-style
//  license that can be found in the LICENSE file.

package expat

import (
	"fmt"
	"runtime"
	"testing"

	"modernc.org/ccgo/v3/lib"
)

func Test(t *testing.T) {
	if _, err := ccgo.Shell("go", "run", fmt.Sprintf("./internal/runtests/main_%s_%s.go", runtime.GOOS, runtime.GOARCH), "--verbose"); err != nil {
		t.Fatal(err)
	}
}
