# expat

CGo-free port of https://libexpat.github.io/

## Installation

    $ go get modernc.org/expat

## Linking using ccgo

    $ ccgo foo.c -lmodernc.org/expat/lib

## Documentation

[godoc.org/modernc.org/expat](http://godoc.org/modernc.org/expat)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2fexpat](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fexpat)
